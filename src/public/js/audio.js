class AudioSource {
    constructor(){
        this.src = ""
        this.stream = new Object()
        this.status = 0
        this.audioPos = 0

        this.load = this.load.bind(this)
        this.play = this.play.bind(this)
        this.stop = this.stop.bind(this)
        this.position = this.position.bind(this)
        this.getStatus = this.getStatus.bind(this)
        this.reset = this.reset.bind(this)
    }

    load(src) {
        this.stream = new Media(src, null, null, this.getStatus)
        this.src = src
    }

    play() {
        this.stream.play()
    }

    stop() {
        this.stream.stop()
        this.stream.release()      
    }

    reset() {
        this.src = ""
        this.stream = new Object()
        this.status = 0
        this.audioPos = 0
    }

    position(obj) {
        this.stream.getCurrentPosition(
            // success callback
            function (position) {
                if (position > -1) {
                     obj.audioPos = position;
                } 
            },
            // error callback
            function (e) {
                alert("Error message = " + e);
            }
        )

        return obj.audioPos
    }

    getStatus(status) {
        this.status = status
    }
}