document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() { 
    let audio = new AudioSource()    
    let pos = 0
    let audioTimer = 0
    let eqStarted = false
    let userAction = false

    startPlayBack = function(url) {   
        userAction = false  
        audio.load(url)
        audio.play()
        timer()    
    }

    startEq = function(){
        if(!eqStarted) {
            $(".bar").each(function(i) {
                fluctuate($(this));  
            });
            eqStarted = true
        }
    }
    
    stopPlayBack = function() {
        audio.stop()          
        stopEq()  
        userAction = true        
    }

    stopEq = function(){
        $(".bar").each(function(i) {
            $(this).clearQueue().stop();
        });
        eqStarted = false
    }
    
    function fluctuate(bar) {
        eqStarted = true
        var hgt = Math.random() * 30;
        hgt += 1;
        var t = hgt * 15;
    
        bar.animate({
            height: hgt
        }, t, function() {
            fluctuate($(this));
        });
    }

    function zeroPad(number) {
        return (number < 10 ? '0' : '') + number
    }    

    function timer() {              
        audioTimer = setInterval(function(){  
            let pos = audio.position(audio)                     
            var s = parseInt(pos % 60);
            var m = parseInt((pos / 60) % 60);
            var h = parseInt(((pos / 60) / 60) % 60);
            connectionIndicator() 
            if (pos > 0 && audio.status === 2) {
                $('#textPosition').text(zeroPad(h) + ':' + zeroPad(m) + ':' + zeroPad(s));
                startEq()                                               
            }           
        }, 500)
    }

    function connectionIndicator() {  
        let message = ""      
        switch(audio.status) {
            case 1:
            message = "connecting: "+audio.status
            setIndicator("#f3f3f3", "#3498db", message)
            break;

            case 2: 
            message = "connected: "+audio.status
            setIndicator("#3498db", "#3498db", message)                
            break;

            case 3:
            message = "Play back Paused: "+audio.status
            setIndicator("#f3f3f3", "#3498db", message)
            break;

            case 4:
            if(!userAction) {
                stopEq()
                startPlayBack(audio.src)
            } else {
                clearInterval(audioTimer)  
                audioTimer = null             
                $('.circle').css('border', '5px solid #f3f3f3'); 
                $('#status').text("Play back stopped: "+audio.status)
            }
            break;
        }
    }

    getAudioStatus = function() {
        return audio.status
    }

    function setIndicator(primaryColor, secondaryColor, message) {
        $('.circle').css('border-right', '5px solid '+primaryColor);
        $('.circle').css('border-bottom', '5px solid '+primaryColor);
        $('.circle').css('border-left', '5px solid '+primaryColor);
        $('.circle').css('border-top', '5px solid '+secondaryColor);            
        $('#status').text(message)
    }
}