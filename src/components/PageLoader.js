import React from 'react'
import { PageNames } from '../actions/page'
import * as pageComponents from '../pages'

const pageMap = {
  [PageNames.Home]: pageComponents.Home,
  [PageNames.Page]: pageComponents.Page,
  [PageNames.Login]: pageComponents.Login
}

const PageSwitcher = ({ page }) => {
    const loadPage = page.page
    const Page = pageMap[loadPage]
    return <Page />
}

export default (page) => <PageSwitcher page={page} />