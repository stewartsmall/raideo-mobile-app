import { combineReducers } from 'redux'
import pages from './pageReducer'
import login from './loginReducer'
import stations from './stationReducer'

const reducers = combineReducers({
  pages,
  login,
  stations
})

export default reducers
