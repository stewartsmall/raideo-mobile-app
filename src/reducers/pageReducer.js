import {NAV_PAGE, NAV_HOME, NAV_LOGIN, PageNames } from '../actions/page'

// action-type/page map
const pages = {
  [NAV_PAGE]: {
    name: PageNames.Page
  },
  [NAV_HOME]: {
      name: PageNames.Home
  },
  [NAV_LOGIN]: {
      name: PageNames.Login
  }
}

export default (state = pages[NAV_HOME], action) => pages[action.type] || state