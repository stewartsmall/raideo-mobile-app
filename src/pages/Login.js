import React from 'react'
import { Button, Input } from 'react-onsenui'
import ons from 'onsenui'
import {connect} from 'react-redux'
import {logInUser} from '../actions/logedIn'
import {navHome} from '../actions/page'

import AnimatedDiv from '../components/PageAnimation'


class Login extends React.Component {
    
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUsernameChange = this.handleUsernameChange.bind(this)
        this.handlePasswordChange = this.handlePasswordChange.bind(this)
    }

    handleUsernameChange(e) {
        this.setState({username: e.target.value})
    }

    handlePasswordChange(e) {
        this.setState({password: e.target.value})
    }

    handleSubmit(dispatch) {  
        if (this.state.username === 'bob' && this.state.password === 'secret') {
            ons.notification.alert('You are now signed in!')
            dispatch(this.props.auth(true))
            dispatch(this.props.goToHome())
        }
        else {
            ons.notification.alert('Username or password incorrect!')
        }
    }

    render() {
        return (
            <section className="center">
                <AnimatedDiv>
                    <h1>Login</h1>

                    <div>
                        <Input
                        value={this.state.username}
                        onChange={this.handleUsernameChange}
                        modifier='underbar'
                        float
                        placeholder='Username' />
                    </div>

                    <br />
                    
                    <div>
                        <Input
                        value={this.state.password}
                        onChange={this.handlePasswordChange}
                        modifier='underbar'
                        type='password'
                        float
                        placeholder='Password' />
                    </div>
                    
                    <br />

                    <div>
                        <Button onClick={() => this.handleSubmit(this.props.dispatch)}>Sign in</Button>
                    </div>
                </AnimatedDiv>
            </section>
        )
    }
}

function mapStateToProps (state) {
    return {
        auth: logInUser,
        goToHome: navHome
    }
}

export default connect(mapStateToProps)(Login)