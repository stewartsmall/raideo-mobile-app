import React from 'react'
// import {Button} from 'react-onsenui'
// import {connect} from 'react-redux'

import AnimatedDiv from '../components/PageAnimation'

class Page extends React.Component {
    render() {
        return(
            <div className="center">
                <AnimatedDiv>
                    <h1>Page</h1>
                </AnimatedDiv>
            </div>
        )
    }
}

export default Page