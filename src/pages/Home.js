import React from 'react'
import {Icon, Fab, Page, Card} from 'react-onsenui'
import ons from 'onsenui'
import EQ from '../components/EQ'
import {connect} from 'react-redux'

import AnimatedDiv from '../components/PageAnimation'

class Home extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            playBtn: 'md-play',
            stream: ''
        }

        this.renderFixed = this.renderFixed.bind(this)
        this.playAudio = this.playAudio.bind(this)
    }

    componentDidMount(){
       
    }

    playAudio(){  
        const { playBtn } = this.state        
        const {stations} = this.props 
        if(getAudioStatus() !== 1) {
            let btn = (playBtn === 'md-play')? 'md-stop' : 'md-play'
            if(btn === 'md-play'){
                stopPlayBack()         
            } else {
                startPlayBack(stations[0].link) 
            } 
            this.setState({
                playBtn: btn
            })
        } else {
            ons.notification.alert("cannot stop audio play back while connecting")
        }        
    }

    switchBtn()
    {    
        const element = document.getElementsByTagName("a")[0]  
        const ele = document.getElementById('audioControl')  
        if(element.getAttribute('class') == 'audioPlay'){
            ele.classList.remove('audioPlay');
            ele.classList.add('audioPause');
            //istream.play();	
        } else {
            ele.classList.add('audioPlay');
            ele.classList.remove('audioPause');
            //istream.stop();
        }        
    }

    renderFixed() {
        let {stations} = this.props
        return (
          <Fab
            style={{backgroundColor: ons.platform.isIOS() ? '#4282cc' : null}}
            onClick={this.playAudio}
            position='bottom center'>
            <Icon icon={this.state.playBtn} />
          </Fab>
        );
      }

    volumeSlider(){
        $("#volume").slider({
            min: 0,
            max: 100,
            value: 10,
            range: "min",
            animate: true,
            slide: function(event, ui) {
                //volumeControl((ui.value) / 100)
                //alert('hi')
            }
        });
    }

    render() {
        let {stations} = this.props        
        return(
            <section className="center">
                <Page
                renderFixed={this.renderFixed}
                >      
                               
                    <div className="content">
                        <div className="absoluteCenter">
                            <div className="circle">
                                <div className="streamInfo">
                                    <h1>{stations[0].name}</h1>
                                    <span>{stations[0].freq} FM</span>
                                </div>
                            </div> 
                            <br />
                            <EQ />
                            <div id="textPosition">00:00:00</div> 
                            <div id="status"></div>                        
                        </div>
                    </div>         

                </Page>
                
            </section>
        )
    }
}

function mapStateToProps(state) {
    return{
        stations: state.stations
    };
}

export default connect(mapStateToProps)(Home)