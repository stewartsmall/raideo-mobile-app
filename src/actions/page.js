// action types
export const NAV_HOME = "NAV_HOME"
export const NAV_PAGE = "NAV_PAGE"
export const NAV_LOGIN = "NAV_LOGIN"

// action creators
export const navHome = () => ({type: NAV_HOME})
export const navPage = () => ({type: NAV_PAGE})
export const navLogin = () => ({type: NAV_LOGIN})

// page names
export const PageNames = {
    Home: "Home",
    Page: "Page",
    Login: "Login"
}

// action-type/page map
const pages = {
    [NAV_PAGE]: {
        name: PageNames.Page
    },
    [NAV_HOME]: {
        name: PageNames.Home
    },
    [NAV_LOGIN]: {
        name: PageNames.Login
    }
}