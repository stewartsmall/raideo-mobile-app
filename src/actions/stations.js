const STATION = "STATION"

export function selectStation(station) {
    return {
        type: 'STATION',
        payload: station
    };
}